global.TextEncoder = require("util").TextEncoder;
global.TextDecoder = require("util").TextDecoder;

require('dotenv').config();
const express = require('express');
const fs = require('fs');
const {ApolloServer, UserInputError} = require('apollo-server-express');
const {GraphQLScalarType} = require('graphql');
const {Kind} = require('graphql/language');
const {MongoClient} = require('mongodb');
const mongoose = require('mongoose');

const url = process.env.DB_URL || 'mongodb://localhost/waitinglist';
const port = process.env.API_SERVER_PORT || 5000;

let db;




const GraphQLDate = new GraphQLScalarType({
    name: 'GraphQLDate',
    description: 'A Date() type in GraphQL as a scalar',
    serialize(value){
        return value.toISOString();
    },
    parseValue(value){
        const dateValue = new Date(value);
        return isNaN(dateValue) ? undefined : dateValue;
    },
    parseLiteral(ast){
        if (ast.kind = Kind.STRING){
            const value = new Date(ast.value);
            return isNaN(value) ? undefined : value;
        }
    },
});

async function waitList(){
    const entries = await db.collection('waitinglist').find({}).toArray();
    return entries.length;
 }




async function getNextSequence(waitlist){
    const result = await db.collection('counters').findOneAndUpdate(
    { _id:waitlist },
    {$inc: {current:1}},
    {returnOriginal: false},
    );
    console.log(result);
    return result.value.current;
}

async function entryAdd(_, {entry}){
    validateEntry(entry);
    entry.date = new Date();
    entry.id = await getNextSequence('waitlist')+1;
    const result = await db.collection('waitinglist').insertOne(entry);
    console.log(result.insertedId);

    const savedEntry = await db.collection('waitinglist').findOne({_id:result.insertedId});
    console.log(savedEntry);
    updateRemainder('remainder',1);
    return savedEntry;
}

async function updateRemainder(remainder,num){
    const result = await db.collection('counters').findOneAndUpdate(
        { _id:remainder },
        {$inc: {remainder:-num}},
        {returnOriginal: false},
        );
        console.log(result);


}

async function entryRemove(_,{entries}){
    let objectIdArray = entries.map(s => mongoose.Types.ObjectId(s));
    console.log(entries);
    console.log(entries.length)
    await db.collection('waitinglist').deleteMany({_id:{$in:objectIdArray}});
    updateRemainder('remainder',-entries.length);


    // return entry;
}

async function remainder(){
    const remainder = await db.collection('counters').find({_id:'remainder'}).toArray();
    return remainder[0].remainder;
}

function validateEntry(entry){

    const errors = [];
    if (entry.name.trim() == "" || entry.number.trim()==""){
        errors.push("Guest Name & Phone Number are both mandatory!");
    }
    else if(/^\d+$/.test(entry.number.trim())==false){
        errors.push("Please enter a valid phone number!");
    }


    if (errors.length > 0){
        throw new UserInputError('Invalid input(s)', {errors});
    }

}

const resolvers ={
    Query:{
        waitList,
        remainder,

    },
    Mutation:{
entryAdd,
entryRemove,
    },
    GraphQLDate,
};



async function connectToDb(){
    const client = new MongoClient(url, {useNewUrlParser: true});
    await client.connect();
    console.log('Connected to MongoDB at', url);
    db = client.db();
}

const server = new ApolloServer({

    typeDefs: fs.readFileSync('schema.graphql', 'utf-8'),
    resolvers,
    formatError: error =>{
        console.log(error);
        return error;
    }

});

const app = express();

// app.use(express.static('public'));

server.applyMiddleware({app, path: '/graphql'});



(async function(){
    try{
        await connectToDb();
        app.listen(port, function() {
            console.log(`API Server started on port ${port}`);
        });
    } catch (err){
        console.log('ERROR:',err);
    }
})();
global.TextEncoder = require("util").TextEncoder;
global.TextDecoder = require("util").TextDecoder;

const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost/waitinglist';
const client = new MongoClient(url, { useNewUrlParser: true });
const db = client.db();
function testWithCallbacks(callback){
    console.log('\n---testWithCallbacks ---');

    client.connect(function(err, client){
        if(err) {
            callback(err);
            return;
        }
        console.log('Connected to MongoDB');

        const db = client.db();
        const collection = db.collection('waitinglist');
        collection.remove({}, function(err,docs){
            if(err){
                client.close();
                cancelIdleCallback(err);
                return;
            }
            console.log('Database Wiped:\n', docs);

        const entry = {id:1, Name:'Russell', Number: '1234567', Date:new Date('2021-09-11T12:08:35')};
        collection.insertOne(entry,function(err,result){
            if(err){
                client.close();
                cancelIdleCallback(err);
                return;
            }
    
            console.log('Result of insert:\n', result.insertedId);
            collection.find({_id: result.insertedId}).toArray(function(err,docs){
                if(err){
                    client.close();
                    callback(err);
                    return;
                }
                console.log('Result of find:\n', docs); 

                collection.deleteOne({_id:result.insertedId},function(err,docs){
                    if(err){
                        client.close();
                        callback(err);
                        return;
                    }
                    console.log('Result of delete:\n', docs, '\n', result.insertedId);
                    client.close();
                    callback(err);
                });

             });
            
        });
    });
    
    
    
    });
    
}


async function testWithAsync(){
    console.log('\n--- testWithAsync ---');
    const client = new MongoClient(url,{useNewUrlParser: true});

    try{
        await client.connect();
        console.log('Connected to MongoDB');
        const db = client.db();
        const collection = db.collection('waitinglist');
        const clear = await collection.remove();
        console.log('Database Wiped\n', clear);
        const entry = {id:2, Name:'Elon', Number: '12345678', Date:new Date('2021-08-11T12:08:35')};
        const result = await collection.insertOne(entry);
        console.log('Result of insert\n:', result.insertedId);
        const docs = await collection.find({_id: result.insertedId}).toArray();
        console.log('Result of find:\n', docs);
        const remove = await collection.deleteOne({_id: result.insertedId});
        console.log('Resulf of delete:\n', remove, '\n', result.insertedId)
    } catch(err){
        console.log(err);
    }finally{
        client.close();
    }

}

testWithCallbacks(function(err){
    if(err){
        console.log(err);
    }
    testWithAsync();
});





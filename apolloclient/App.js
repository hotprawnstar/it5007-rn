/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';


import Query from './data/query';
import {
  ScrollView,
  View,
  Text,
  RefreshControl,
} from 'react-native';
import { withStatement } from '@babel/types';

// Initialize Apollo Client
const client = new ApolloClient({
  uri:'http://10.0.2.2:5000/graphql',
  cache: new InMemoryCache()
});

const wait = (timeout) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

class App extends Component {
  constructor(props){
    super(props)
    this.state={
      refreshing:false
    }
    this._onRefresh = this._onRefresh
  
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    wait(2000).then(() => {
      this.setState({refreshing: false});
    });
  }


render(){

  return (
<ApolloProvider client={client}>
       <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} />}>
        <View>
      </View>
      <View ><Query refresh={this.state.refreshing}/></View>
      </ScrollView>
      </ApolloProvider>
  )
    };
};


export default App;

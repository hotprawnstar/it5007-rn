import React from 'react';
import { Text, View } from 'react-native';
import { useQuery, gql } from '@apollo/client';



const DataList = ({data}) => {
    
    console.log(data)
  return (
<View>
          { data.map((cust, index) => {
              return (
                <View key = {cust._id}>
                <Text>Index: {index + 1}</Text>
                <Text>Name: {cust.name}</Text>
                <Text>Number: {cust.number}</Text>
                <Text>Date: {cust.date}</Text>
                <Text></Text>
                </View>
              );
          })}</View>
    
  );
}

export default DataList;


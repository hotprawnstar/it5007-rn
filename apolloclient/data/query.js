import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { useQuery, gql } from '@apollo/client';
import DataList from './data';


const remainder = gql`query{
    waitList{
        _id
        name
        number
        date
    }
}`;;

const Query = props => {
  const {loading, error, data, refetch, networkStatus} = useQuery(remainder,{fetchPolicy:"cache-and-network"});
  if (props.refresh){
    refetch();
  }
  console.log(networkStatus);
  if (networkStatus == 4) return <Text>Reloading...</Text>;
  if (loading) return <Text>Loading...</Text>;
  if (error) return <Text>Error loading API</Text>;
  if (data.waitList.length==0) return <Text>No entries in waitlist</Text>

  return (
      <View>

    <DataList data={data.waitList} />
    </View>
  );
}

export default Query;